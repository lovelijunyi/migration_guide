# 介绍

## 简要介绍

Flask是一个使用 Python 编写的轻量级 Web 应用框架。其 WSGI 工具箱采用 Werkzeug ，模板引擎则使用 Jinja2 。
本案例使用x86_64架构虚拟机，通过评估工具x2openEuler评估flask 1.1.2软件移植到openEuler操作系统的兼容性，并根据评估结果完成软件移植。

开发语言：Python

开源协议：BSD

## 建议的版本

建议使用版本为flask 1.1.2。

> 说明:
> 本文档适用于flask 1.1.2，其他版本的flask移植步骤也可参考本文档。

# 环境要求

## 操作系统要求

| 操作系统  | 版本          |
| :-------- | :------------ |
| openEuler | 20.03 LTS SP1 |
| CentOS    | 7.6           |

## 安装操作系统

如果是全新安装操作系统，安装方式建议不要使用最小化安装，否则很多软件包需要手动安装，可选择“Server with GUI”安装方式。
安装openEuler操作系统请参考：[https://openeuler.org/zh/docs/20.03_LTS_SP1/docs/Installation/installation.html。](https://bbs.huaweicloud.com/forum/thread-116157-1-1.html#)

# 兼容性评估

## 获取flask的RPM包

```
wget https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/p/python36-flask-1.1.2-4.el7.noarch.rpm
```

## 获取工具 x2openEuler

```
xxx
```

## 配置工具

```
   rpm -ivh x2openEuler-1.0-1.noarch.rpm
   su x2openEuler
   cd 
   x2openEuler redis-db -init       
   x2openEuler使用rpm安装完成后会在/opt/x2openEuler目录下带有source_centos7.6-openEuler20.03-LTS-SP1.tar.gz这个默认资源包
   需要支持centos8.2到openEuler20.03-LTS-SP1的评估，则需获取对应的静态资源包导入，如对应的资源包为source_centos8.2-openEuler20.03-LTS-SP1.tar.gz，导入此                
   包命令：`x2openEuler init source_centos8.2-openEuler20.03-LTS-SP1.tar.gz`,视情况选择对应的资源包
```

## 运行x2openEuler工具

```
x2openEuler scan /root/python36-flask-1.1.2-4.el7.noarch.rpm
```

## 查看结果

# 安装flask

## rpm安装

由于兼容性报告显示兼容，尝试直接用下载的rpm包安装。

```
[root@localhost test]# yum install -y python36-flask-1.1.2-4.el7.noarch.rpm
Last metadata expiration check: 1:39:08 ago on Mon 22 Mar 2021 10:35:29 AM CST.
Error:
 Problem: conflicting requests
  - nothing provides python36-setuptools needed by python36-flask-1.1.2-4.el7.noarch
  - nothing provides python(abi) = 3.6 needed by python36-flask-1.1.2-4.el7.noarch
  - nothing provides python36-click >= 5.1 needed by python36-flask-1.1.2-4.el7.noarch
  - nothing provides python36-itsdangerous >= 0.24 needed by python36-flask-1.1.2-4.el7.noarch
  - nothing provides python36-jinja2 >= 2.10.1 needed by python36-flask-1.1.2-4.el7.noarch
  - nothing provides python36-werkzeug >= 0.15 needed by python36-flask-1.1.2-4.el7.noarch
(try to add '--skip-broken' to skip uninstallable packages or '--nobest' to use not only best candidate packages)
```

由于依赖原因未能安装。

## pip方式安装

使用pip安装同版本的flask，由于flask依赖python3，使用pip3。

```
[root@localhost ~]# pip3 install flask
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Collecting flask
  Using cached Flask-1.1.2-py2.py3-none-any.whl (94 kB)
Requirement already satisfied: itsdangerous>=0.24 in /usr/lib/python3.7/site-packages (from flask) (1.1.0)
Requirement already satisfied: Werkzeug>=0.15 in /usr/local/lib/python3.7/site-packages (from flask) (1.0.1)
Requirement already satisfied: click>=5.1 in /usr/local/lib/python3.7/site-packages (from flask) (7.1.2)
Requirement already satisfied: Jinja2>=2.10.1 in /usr/lib/python3.7/site-packages (from flask) (2.11.2)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/lib64/python3.7/site-packages (from Jinja2>=2.10.1->flask) (1.1.1)
Installing collected packages: flask
Successfully installed flask-1.1.2
```

# 运行和验证

## 检查版本

```
[root@localhost ~]# python3
Python 3.7.9 (default, Dec 16 2020, 03:16:57)
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import flask
>>> flask.__version__
'1.1.2'
```

## 使用flask

使用flask官网 ([https://flask.palletsprojects.com/en/1.1.x/quickstart/#a-minimal-application](https://bbs.huaweicloud.com/forum/thread-115817-1-1.html#)) 提供的入门程序

```
vim hello.py
```

编辑内容如下:

```
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'
```

编辑后保存退出，在终端输入如下命令：

```
[root@localhost ~]# export FLASK_APP=hello.py
[root@localhost ~]# python3 -m flask run
 * Serving Flask app "hello.py"
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

可见运行成功。